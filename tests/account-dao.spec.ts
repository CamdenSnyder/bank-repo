import { conn } from "../src/connection";
import { AccountDAO } from "../src/daos/account-dao";
import { AccountDaoPostgres } from "../src/daos/account-dao-postgres";
import { Account, Client } from "../src/entities";
import { ClientDaoPostgres } from "../src/daos/client-dao-postgres";
import { ClientDAO } from "../src/daos/client-dao";


const accountDAO:AccountDAO = new AccountDaoPostgres();
const clientDAO:ClientDAO = new ClientDaoPostgres();

const testAccount:Account = new Account(1, 3000, "Checking", 1);

test("Create an account", async ()=>{
    let client1:Client = new Client(0, "Camden", "Snyder");
    client1 = await clientDAO.createClient(client1);
    let client2:Client = new Client(0, "Camden", "Snyder");
    client2 = await clientDAO.createClient(client2);

    const result:Account = await accountDAO.createAccount(testAccount);
    console.log(result);
    expect(result.accountId).not.toBe(0);
});

test("Get account by Id", async ()=>{
    let account:Account = new Account(0, 1111, "Checking", 1);
    account = await accountDAO.createAccount(account);

    let retrievedAccount:Account = await accountDAO.getAccountById(account.accountId);

    expect(retrievedAccount.amount).toBe(account.amount);
});

test("Get all accounts", async ()=>{
    let account1:Account = new Account(0, 1000, "Checking", 1);
    let account2:Account = new Account(0, 2250, "Savings", 1);
    let account3:Account = new Account(0, 3500, "Checking", 1);
    await accountDAO.createAccount(account1);
    await accountDAO.createAccount(account2);
    await accountDAO.createAccount(account3);

    const accounts:Account[] = await accountDAO.getAllAccounts();

    expect(accounts.length).toBeGreaterThanOrEqual(3);
})


test("Get accounts by Cid", async ()=>{
    let account1:Account = new Account(0, 1000, "Checking", 1);
    let account2:Account = new Account(0, 2250, "Savings", 1);
    let account3:Account = new Account(0, 3500, "Checking", 1);
    await accountDAO.createAccount(account1);
    await accountDAO.createAccount(account2);
    await accountDAO.createAccount(account3);

    const accounts:Account[] = await accountDAO.getAccountsByCid(1);

    expect (accounts.length).toBeGreaterThanOrEqual(3);
});

test("Deposit into account", async()=>{
    let account:Account = new Account(0, 4000, "Checking", 1);
    account = await accountDAO.createAccount(account);

    const retrievedAccount:Account = await accountDAO.accountDeposit(account.accountId, 500);

    expect (retrievedAccount.amount).toBe(4500);
})

test("Withdraw from account", async ()=>{
    let account:Account = new Account(0, 4000, "Checking", 1);
    account = await accountDAO.createAccount(account);

    const retrievedAccount:Account = await accountDAO.accountWithdraw(account.accountId, 500);

    expect (retrievedAccount.amount).toBe(3500);
});

test("Update account", async ()=>{
    let account:Account = new Account(0,6000,"Savings",1);
    account = await accountDAO.createAccount(account);

    // to update an object we just edit it and then pass it into a method
    account.amount = 5000;
    account.accType = "Checking";
    account = await accountDAO.updateAccount(account);

    const updatedAccount = await accountDAO.getAccountById(account.accountId);
    expect(updatedAccount.amount).toBe(5000);
});

test("Delete account by id", async ()=>{
    let account:Account = new Account(0,7000,"Savings",1);
    account = await accountDAO.createAccount(account);

    const result:boolean = await accountDAO.deleteAccountById(account.accountId);
    expect(result).toBeTruthy()

});

afterAll(async()=>{
    conn.end();
});
