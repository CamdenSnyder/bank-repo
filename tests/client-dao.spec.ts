import { conn } from "../src/connection";
import { ClientDAO } from "../src/daos/client-dao";
import { ClientDaoPostgres } from "../src/daos/client-dao-postgres";
import { Client } from "../src/entities";


const clientDAO:ClientDAO = new ClientDaoPostgres();

const testClient:Client = new Client(0, "Camden", "Snyder");

test("Create a Client", async ()=>{
    const result:Client = await clientDAO.createClient(testClient);
    //console.log(result);
    expect(result.clientId).not.toBe(0);
});

test("Get client by Id", async ()=>{
    let client:Client = new Client(1, "Camden", "Snyder");
    client = await clientDAO.createClient(client);

    let retrievedClient:Client = await clientDAO.getClientById(client.clientId)

    expect(retrievedClient.fname).toBe(client.fname);
});

test("Get all clients", async ()=>{
    let client1:Client = new Client(0, "Joe", "Shmoe");
    let client2:Client = new Client(0, "Jim", "Bob");
    let client3:Client = new Client(0, "Camden", "Snyder");
    await clientDAO.createClient(client1);
    await clientDAO.createClient(client2);
    await clientDAO.createClient(client3);

    const clients:Client[] = await clientDAO.getAllClients();

    expect(clients.length).toBeGreaterThanOrEqual(3);
});

test("Update client", async ()=>{
    let client:Client = new Client(0,'Jimmy','Jimjim');
    client = await clientDAO.createClient(client);

    // to update an object we just edit it and then pass it into a method
    client.fname = "Jim";
    client.lname = "Jimmyjim";
    client = await clientDAO.updateClient(client);

    const updatedClient = await clientDAO.getClientById(client.clientId);
    expect(updatedClient.fname).toBe("Jim");
});

test("Delete client by id", async ()=>{
    let client:Client = new Client(0,'Good','Bye');
    client = await clientDAO.createClient(client);

    const result:boolean = await clientDAO.deleteClientById(client.clientId);
    expect(result).toBeTruthy()

});


afterAll(async()=>{
    conn.end();
});