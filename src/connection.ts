import {Client} from 'pg';
require('dotenv').config({path:'D:\\Revature\\Project0\\app.env'})

export const conn = new Client({
    user:'postgres',
    password:process.env.DBPASSWORD, // YOU SHOULD NEVER STORE PASSWORDS IN CODE
    database:process.env.DATABASENAME,
    port:5432,
    host:'34.138.96.242'
})
conn.connect();