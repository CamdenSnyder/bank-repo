import express from 'express';
import { Client, Account } from './entities';
import { MissingResourceError } from './errors';
import ClientService from './services/client-service';
import { ClientServiceImpl } from './services/client-service-impl';
import AccountService from './services/account-service';
import { AccountServiceImpl } from './services/account-service.impl';

const app = express();
app.use(express.json());


const clientService:ClientService = new ClientServiceImpl();
const accountService:AccountService = new AccountServiceImpl();

// CLIENTS

// Get clients
app.get("/clients", async (req, res)=>{
    const clients:Client[] = await clientService.retrieveAllClients();
    res.send(clients);
    res.status(200)
});

// Get client by Id
app.get("/clients/:id", async (req, res) =>{
    try{
        const clientId = Number(req.params.id);
        const client:Client = await clientService.retrieveClientById(clientId);
        res.send(client);
    }catch (error) {
        if(error instanceof MissingResourceError){
            res.status(404);
            res.send(error);
        }
    }
});

// Post client
app.post("/clients", async (req, res) =>{
    let client:Client = req.body;
    client = await clientService.registerClient(client);
    res.status(201);
    res.send(client);

});

// Put client
app.put("/clients", async (req, res) =>{
    try{
        const newClient:Client = req.body;
        const client:Client = await clientService.modifyClient(newClient);
        res.send(client);
    }catch(error){
        if(error instanceof MissingResourceError){
            res.status(404);
            res.send(error);
        }
    }
})

// Delete client
app.delete("/clients/:id", async (req, res) =>{
    try{
        const clientId = Number(req.params.id);
        const success:Boolean = await clientService.removeClientById(clientId);
        res.status(205);
        res.send(success);
    }catch(error){
        if(error instanceof MissingResourceError){
            res.status(404);
            res.send(error);
        }
    }
});


// ACCOUNTS

// Get accounts
app.get("/accounts", async (req, res)=>{
    const accounts:Account[] = await accountService.retrieveAllAccounts();
    res.send(accounts);
    res.status(200)
});

// Get account by Id
app.get("/accounts/:id/Id", async (req, res) =>{
    try{
        const accountId = Number(req.params.id);
        const account:Account = await accountService.retrieveAccountById(accountId);
        res.send(account);
    }catch (error) {
        if(error instanceof MissingResourceError){
            res.status(404);
            res.send(error);
        }
    }
});

// Get accounts by Cid
app.get("/accounts/:cid/Cid", async (req, res) =>{
    try{
        const accountCid = Number(req.params.cid);
        const accounts:Account[] = await accountService.retrieveAccountsByCid(accountCid);
        res.send(accounts);
    }catch (error) {
        if(error instanceof MissingResourceError){
            res.status(404);
            res.send(error);
        }
    }
});

// Post account
app.post("/accounts", async (req, res) =>{
    let account:Account = req.body;
    account = await accountService.registerAccount(account);
    res.send(account);
    res.status(201);
});

// Put account
app.put("/accounts", async (req, res) =>{
    try{
        const newAccount:Account = req.body;
        const account:Account = await accountService.modifyAccount(newAccount);
        res.send(account);
    }catch(error){
        if(error instanceof MissingResourceError){
            res.status(404);
            res.send(error);
        }
    }
})

// Patch account (deposit)
app.patch("/accounts/:id/deposit", async (req, res)=>{
    try{
        const accountId = Number(req.params.id);
        const amountUpdate = Number(req.body.amount)
        const account:Account = await accountService.depositByAccountId(accountId,amountUpdate);
        res.send(account);
    }catch(error){
        res.status(404);
        res.send(error);
    }
});

//Patch account (withdraw)
app.patch("/accounts/:id/withdraw", async (req, res)=>{
    try{
        const accountId = Number(req.params.id);
        const amountUpdate = Number(req.body.amount)
        const account = await accountService.withdrawByAccountId(accountId,amountUpdate);
        res.send(account);
    }catch(error){
        res.status(404);
        res.send(error);
    }
});

// Delete account
app.delete("/accounts/:id", async (req, res) =>{
    //const accountId = Number(req.params.id);
    try{
        const accountId = Number(req.params.id);
        const success:Boolean = await accountService.removeAccountById(accountId);
        res.send(success);
    }catch(error){
        if(error instanceof MissingResourceError){
            res.status(404);
            res.send(error);
        }
    }
});




app.listen(3000, ()=>{console.log("Application Started")})