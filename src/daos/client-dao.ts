import { Client } from "../entities";


export interface ClientDAO{


    createClient(client:Client):Promise<Client>;

    getAllClients():Promise<Client[]>;

    getClientById(clientId:number):Promise<Client>;

    updateClient(client:Client):Promise<Client>;

    deleteClientById(client:number):Promise<boolean>;
}