import { Account } from "../entities";


export interface AccountDAO{


    createAccount(account:Account):Promise<Account>;

    getAllAccounts():Promise<Account[]>;

    getAccountById(accountId:number):Promise<Account>;

    getAccountsByCid(accountCid:number):Promise<Account[]>;

    accountDeposit(accountId:Number, amount:Number);

    accountWithdraw(accountId:Number, amount:Number);

    updateAccount(account:Account):Promise<Account>;

    deleteAccountById(account:number):Promise<boolean>;
}