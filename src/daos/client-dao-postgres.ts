import { Client } from "../entities";
import { ClientDAO } from "./client-dao";
import { conn } from "../connection";
import { MissingResourceError } from "../errors";

export class ClientDaoPostgres implements ClientDAO{

    async createClient(client: Client): Promise<Client> {
        const sql:string = "insert into client(fname, lname) values ($1,$2) returning client_id";
        const values = [client.fname, client.lname];
        const result = await conn.query(sql, values);
        client.clientId = result.rows[0].client_id;
        return client;
    }

    async getAllClients(): Promise<Client[]> {
        const sql:string = 'select * from client';
        const result = await conn.query(sql);
        const clients:Client[] = [];
        for(const row of result.rows){
            const client:Client = new Client(
                row.client_id,
                row.fname,
                row.lname);
            clients.push(client);
        }
        return clients;
    }

    async getClientById(clientId: number): Promise<Client> {
        const sql:string = 'select * from client where client_id = $1';
        const values = [clientId];
        const result = await conn.query(sql, values);
        if(result.rowCount === 0){
            throw new MissingResourceError(`The client with id ${clientId} does not exist`);
        }
        const row = result.rows[0];
        const client:Client = new Client(
                row.client_id,
                row.fname,
                row.lname);
        return client;
    }

    async updateClient(client: Client): Promise<Client> {
        const sql:string = 'update client set fname = $1, lname = $2 where client_id = $3'
        const values = [client.fname, client.lname, client.clientId];
        const result = await conn.query(sql, values);
        if(result.rowCount === 0){
            throw new MissingResourceError(`The client with id ${client.clientId} does not exist`);
        }
        return client;
    }

    async deleteClientById(clientId: number): Promise<boolean> {
        const sql:string = 'delete from client where client_id = $1';
        const values = [clientId];
        const result = await conn.query(sql, values);
        if(result.rowCount === 0){
            throw new MissingResourceError(`The book with id ${clientId} does not exist`);
        }
        return true;
    }
}