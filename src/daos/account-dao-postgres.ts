import { Account } from "../entities";
import { AccountDAO } from "./account-dao";
import { conn } from "../connection";
import { InsufficientFundsError, MissingResourceError } from "../errors";

export class AccountDaoPostgres implements AccountDAO{

    async createAccount(account: Account): Promise<Account> {
        const sql:string = "insert into account(amount,acc_type,c_id) values ($1,$2,$3) returning account_id";
        const values = [account.amount, account.accType, account.cId];
        const result = await conn.query(sql, values);
        account.accountId = result.rows[0].account_id;
        return account;
    }

    async getAllAccounts(): Promise<Account[]> {
        const sql:string = 'select * from account';
        const result = await conn.query(sql);
        const accounts:Account[] = [];
        for(const row of result.rows){
            const account:Account = new Account(
                row.account_id,
                row.amount,
                row.acc_type,
                row.c_id);
            accounts.push(account);
        }
        return accounts;
    }

    async getAccountById(accountId: number): Promise<Account> {
        const sql:string = 'select * from account where account_id = $1';
        const values = [accountId];
        const result = await conn.query(sql, values);
        if(result.rowCount === 0){
            throw new MissingResourceError(`The account with id ${accountId} does not exist`);
        }
        const row = result.rows[0];
        const account:Account = new Account(
                row.account_id,
                row.amount,
                row.acc_type,
                row.c_id);
        return account;
    }

    async getAccountsByCid(accountCid: number): Promise<Account[]> {
        const sql:string = 'select * from account where c_id = $1';
        const values = [accountCid];
        const result = await conn.query(sql, values);
        if(result.rowCount === 0){
            throw new MissingResourceError(`There are no accounts with cId of ${accountCid}`)
        }
        const accounts:Account[] = [];
        for(const row of result.rows){
            const account:Account = new Account(
                row.account_id,
                row.amount,
                row.acc_type,
                row.c_id);
                accounts.push(account);
        }
        return accounts;
    }

    async accountDeposit(accountId:number, amount:number): Promise<Account>{
        const sql:string = 'select * from account where account_id = $1';
        const values = [accountId];
        const result = await conn.query(sql,values);
        if (result.rowCount === 0) {
            throw new MissingResourceError(`The account with id ${accountId} does not exist`)
        }
        const row = result.rows[0];
        amount = row.amount + amount;
        const account:Account = new Account(
            row.account_id,
            amount,
            row.acc_type,
            row.c_id);
        return account;
    }

    async accountWithdraw(accountId:number, amount:number){
        const sql:string = 'select * from account where account_id = $1';
        const values = [accountId];
        const result = await conn.query(sql,values);
        if (result.rowCount === 0) {
            throw new MissingResourceError(`The account with id ${accountId} does not exist`)
        }
        const row = result.rows[0];
        amount = row.amount - amount;
        if(amount < 0){
            throw new InsufficientFundsError(`The account with id ${accountId} lacked sufficient funds`)
        }
        const account:Account = new Account(
            row.account_id,
            amount,
            row.acc_type,
            row.c_id);
        return account;
    }


    async updateAccount(account:Account): Promise<Account> {
        const sql:string = 'update account set amount = $1, acc_type = $2, c_id = $3 where account_id = $4'
        const values = [account.amount, account.accType, account.cId, account.accountId];
        const result = await conn.query(sql, values);
        if(result.rowCount === 0){
            throw new MissingResourceError(`The account with id ${account.accountId} does not exist`);
        }
        return account;
    }

    async deleteAccountById(accountId:number): Promise<boolean> {
        const sql:string = 'delete from account where account_id = $1';
        const values = [accountId];
        const result = await conn.query(sql, values);
        if(result.rowCount === 0){
            throw new MissingResourceError(`The book with id ${accountId} does not exist`);
        }
        return true;
    }
}