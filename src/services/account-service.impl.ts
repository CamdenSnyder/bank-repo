import { AccountDAO } from "../daos/account-dao";
import { AccountDaoPostgres } from "../daos/account-dao-postgres";
import { Account } from "../entities";
import AccountService from "./account-service";





export class AccountServiceImpl implements AccountService{

    accountDAO:AccountDAO = new AccountDaoPostgres();

    registerAccount(account: Account): Promise<Account> {
        return this.accountDAO.createAccount(account);
    }

    retrieveAllAccounts(): Promise<Account[]> {
        return this.accountDAO.getAllAccounts();
    }
    
    retrieveAccountsByCid(cId: number): Promise<Account[]> {
        return this.accountDAO.getAccountsByCid(cId);
    }

    retrieveAccountById(accountId: number): Promise<Account> {
        return this.accountDAO.getAccountById(accountId);
    }

    depositByAccountId(id:number, amount:number): Promise<Account> {
        return this.accountDAO.accountDeposit(id, amount);
    }

    withdrawByAccountId(id:number, amount:number): Promise<Account> {
        return this.accountDAO.accountWithdraw(id, amount);
    }

    modifyAccount(account: Account): Promise<Account> {
        return this.accountDAO.updateAccount(account);
    }
    
    removeAccountById(accountId: number): Promise<boolean> {
        return this.accountDAO.deleteAccountById(accountId);
    }

}