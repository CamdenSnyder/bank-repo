import { Client } from "../entities";



export default interface ClientService{

    registerClient(client:Client):Promise<Client>;

    retrieveAllClients():Promise<Client[]>;

    retrieveClientById(clientId:number):Promise<Client>;

    modifyClient(client:Client):Promise<Client>;

    removeClientById(clientId:number):Promise<boolean>;
}