import { Account } from "../entities";



export default interface AccountService{

    registerAccount(account:Account):Promise<Account>;

    retrieveAllAccounts():Promise<Account[]>;

    retrieveAccountsByCid(cId:number):Promise<Account[]>;

    retrieveAccountById(accountId:number):Promise<Account>;

    depositByAccountId(id:Number, amount:Number):Promise<Account>;

    withdrawByAccountId(id:Number, amount:Number):Promise<Account>;

    modifyAccount(account:Account):Promise<Account>;

    removeAccountById(accountId:number):Promise<boolean>;
}