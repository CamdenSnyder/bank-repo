import { ClientDAO } from "../daos/client-dao";
import { ClientDaoPostgres } from "../daos/client-dao-postgres";
import { Client } from "../entities";
import ClientService from "./client-service";




export class ClientServiceImpl implements ClientService{

    clientDAO:ClientDAO = new ClientDaoPostgres();

    registerClient(client: Client): Promise<Client> {
        return this.clientDAO.createClient(client);
    }

    retrieveAllClients(): Promise<Client[]> {
        return this.clientDAO.getAllClients();
    }

    retrieveClientById(clientId: number): Promise<Client> {
        return this.clientDAO.getClientById(clientId);
    }

    modifyClient(client: Client): Promise<Client> {
        return this.clientDAO.updateClient(client);
    }

    removeClientById(clientId: number): Promise<boolean> {
        return this.clientDAO.deleteClientById(clientId);
    }
}