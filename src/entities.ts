
export class Client{
    constructor(
        public clientId:number,
        public fname:string,
        public lname:string
    ){}
}

export class Account{
    constructor(
        public accountId:number,
        public amount:number,
        public accType:string,
        public cId:number
    ){}
}